FROM python:2.7-alpine

COPY demoapp/ demoapp/

CMD ["python", "demoapp/server.py"]

EXPOSE 7272